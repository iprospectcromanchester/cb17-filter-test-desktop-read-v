import DDLtrackCampaign from "../common";
import { pollFor } from "icarus";
import { toArray } from "../functions/helpers";
import startFilterBarObserver from "./filter-bar-observer";

export default function initCommonTracking(variation, label) {
  init(variation, label)
  startFilterBarObserver(init, variation, label);
}

function init(variation, label) {
  pollFor(".itemHeader.js-expand-refine", addCategoriesTracking);
  pollFor("input.js-apply-filter", addAppliedFiltersTracking);
  pollFor(".applySort.js-apply-sort", addSortSelectTracking);
  pollFor(".btn.loadMoreProducts", addLoadMoreBtnTracking);

  function addCategoriesTracking() {
    trackCategories(variation, label);
  }

  function addAppliedFiltersTracking() {
    trackFilterApplied(variation, label);
  }

  function addSortSelectTracking() {
    trackSortSelect(variation, label);
  }

  function addLoadMoreBtnTracking() {
    trackLoadMoreBtn(variation, label);
  }
}

function trackCategories(variation, label) {
  let categoryHeaders = toArray(
    document.querySelectorAll(".itemHeader.js-expand-refine")
  );
  categoryHeaders.forEach((header) => {
    header.addEventListener("click", () =>
      DDLtrackCampaign(variation, "Filter Clicked", label)
    );
  });
}

function trackFilterApplied(variation, label) {
  let checkBoxes = toArray(document.querySelectorAll("input.js-apply-filter"));
  checkBoxes.forEach((box) => {
    box.addEventListener("click", () => {
      trackFilter(box, variation, label);
    });
  });
}

function trackFilter(box, variation, label) {
  let filterCategoryName = box.parentElement.parentElement.parentElement.previousElementSibling.innerText.toLowerCase();
  box.checked
    ? DDLtrackCampaign(
        variation,

        `Filter Applied ${filterCategoryName}`,
        label
      )
    : DDLtrackCampaign(
        variation,

        `Filter Removed ${filterCategoryName}`,
        label
      );
}

function trackSortSelect(variation, label) {
  let select = document.querySelector(".applySort.js-apply-sort");
  let options = toArray(select.querySelectorAll("option"));

  select.addEventListener("click", () => {
    DDLtrackCampaign(variation, "Sort by clicked", label);
  });

  select.addEventListener("change", () => {
    let optionSelectedText = getOptionTextByValue(select.value, options);

    DDLtrackCampaign(variation, `${optionSelectedText}`, "CRO sort by");
  });
}

function getOptionTextByValue(value, options) {
  return options.filter((option) => {
    return option.value === value;
  })[0].innerText;
}

function trackLoadMoreBtn(variation, label) {
  let loadMoreBtn = document.querySelector(".btn.loadMoreProducts");

  loadMoreBtn.addEventListener("click", () => {
    DDLtrackCampaign(variation, "Load More", label);
  });
}
