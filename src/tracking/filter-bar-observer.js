import {toArray} from "../functions/helpers";
import { pollFor } from "icarus";

export default function startFilterBarObserver(cb, variation, label) {
  console.log("observing")
    let filterBar = document.querySelector(".sidebar");
    const config = { attributes: false, childList: true, subtree: true };
  
    const firstSelectObserver = new MutationObserver((value) => {
      value.forEach((record) => {
        let addedNodesClassNames = toArray(record.addedNodes).map((node) => {
          return node.className;
        });
        if (addedNodesClassNames.indexOf("refine js-panel") > -1) {
        cb(variation, label)
        }
      });
    });
  
    firstSelectObserver.observe(filterBar, config);
  }