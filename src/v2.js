import DDLtrackCampaign from "./common.js";
import { pollFor } from "icarus";
import initChanges from "./functions/init-changes";



import "./v2.scss";

const testVar = "Variation 2";

pollFor("body", initCb17);

function initCb17() {
  if (document.body.className.indexOf("cb17_loaded") === -1) {
    DDLtrackCampaign(testVar); // general campaign tracking
    cb17Changes();
  } else {
    console.warn("Experiment not loaded");
  }
}

function cb17Changes() {
  document.body.classList.add("cb17_loaded");

  // your test changes go here */

initChanges()



}


