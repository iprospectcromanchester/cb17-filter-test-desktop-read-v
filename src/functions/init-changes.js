import { pollFor } from "icarus";
import { isIE, appExists } from "./helpers";
import createFiltersBlock from "./filters-layout";
import { addEventToSelectOptions } from "./select-options-event.js";
import addClearAllBtnToOptionLists from "./select-options-clear-all-btn";
import { addCustomProductGrid } from "./products-grid";
import addEventToSelectHeaders from "./select-headers-event";
import addAppliedAmountToHeader from "./select-headers-numbers";
import changeArrows from "./change-arrows";
import addCustomCheckboxes from "./select-options-checkboxes";
import { createLazyImgObserver } from "./image-intersection-observer";
import {startGridObserver} from "./product-grid-observer";
import startSidebarObserver from "./sidebar-observer";
import startBodyObserver from "./body-observer";
import stickyScrollObserver from "./sticky-filter-bar";
import hideLabelsInSafariHashBack from "./safariHashBack"

export default function initChanges() {
  const mainContent = document.querySelector(".col.mainContent");
  const lazyImages = document.querySelectorAll(".lazyUnveil");

  pollFor("input.js-apply-filter", addCustomCheckboxes);
  pollFor(appExists, addFilterBlock);
  pollFor(appExists, () => {
    addEventToSelectOptions(mainContent);
  });

  pollFor("div.refineItemOptions", addClearAllBtnToOptionLists);
  pollFor(".productGrid", addCustomProductGrid);
  pollFor(".icon-plus", addEventToSelectHeaders);
  pollFor(".icon-plus", changeArrows);

  addAppliedAmountToHeader();
  if (isIE() === false) {
    createLazyImgObserver(lazyImages);
  }
  pollFor(".productGrid", startGridObserver);
  startSidebarObserver();
  startBodyObserver();
  stickyScrollObserver();



  pollFor(".productGrid", hideLabelsInSafariHashBack);



}

function addFilterBlock() {
  const mainContentWrap = document.querySelector(".catContentWrapper");
  createFiltersBlock(mainContentWrap);
}
