import { createElement, toArray } from "./helpers";

export default function createBackToPeviuosItemLink() {
  let backBtn = createElement("div", "goBackBtn");
  let breadcrumb = document.querySelector(".breadcrumb");
  let bcArr = toArray(breadcrumb.querySelectorAll("li"));


  if (bcArr.length > 2) {
    let previousItem = bcArr.splice(bcArr.length - 2, 1)[0];
    backBtn.innerHTML = previousItem.innerHTML;

    return changeCopy(backBtn)
  } else {
    return backBtn = false;
  }
  


function changeCopy(backBtn) {
  let span = backBtn.querySelector("span")
  span.innerHTML = "Return to " + span.innerHTML;

  return backBtn
}
  
}