import { toArray, createElement } from "./helpers";

export default function addClearAllBtnToOptionLists() {
  const lists = document.querySelectorAll("div.refineItemOptions");

  lists.forEach((list) => {
    if (list.querySelector(".filer-options-clear-all")) return;
    const clearAllBtn = createElement("div", [
      "filer-options-clear-all",
      "ddl_hide",
    ]);
    clearAllBtn.innerHTML = "<span>Clear all</span>";

    clearAllBtn.addEventListener("click", (e) => {
      clearFiltersAndLabel(list.parentElement.className);
    });

    list.insertBefore(clearAllBtn, list.firstElementChild);
  });
}

function clearFiltersAndLabel(listClassName) {
  let labels = toArray(document.querySelectorAll(".filterLabel"));

  labels.forEach((label) => {
    if (listClassName.indexOf(label.dataset.parent) > -1) {
      label.click();
    }
  });
}
