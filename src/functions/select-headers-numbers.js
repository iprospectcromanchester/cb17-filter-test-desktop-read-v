import { toArray } from "./helpers";

export default function addAppliedAmountToHeader() {
  let form = document.querySelector("form[name = refine]");
  let lists = toArray(form.querySelectorAll(".refineListItem"));

  lists.forEach((list) => {
    let header = list.querySelector(".itemHeader");
    let checkBoxes = toArray(list.querySelectorAll(".js-apply-filter"));
    let checked = checkBoxes.filter((box) => {
      return box.checked;
    });
    let span = header.querySelector("span");
    let clearAll = list.querySelector(".filer-options-clear-all")

    if (checked.length > 0) {
      span.innerText = `${span.innerText.replace(/\(.*$/, "")} (${checked.length})`;
      span.classList.add("ddl_bold");
      clearAll.classList.remove("ddl_hide")
    } else {
      span.classList.remove("ddl_bold");
    }
  });
}
