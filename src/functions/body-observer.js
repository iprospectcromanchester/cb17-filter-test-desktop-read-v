import initChanges from "./init-changes";
import { pollFor } from "icarus";

export default function startBodyObserver() {
  let breadcumbs1 = document.querySelector("div.js-breadcrumb");
  let breadCrumList1 = breadcumbs1.querySelector("ol");
  const body = document.querySelector("body");
  const config = { attributes: false, childList: true, subtree: false };

  const mainContentObserver = new MutationObserver((value) => {
    let breadcumbs = document.querySelector("div.js-breadcrumb");
    let breadCrumList2 = breadcumbs.querySelector("ol");

    if (
      breadCrumList1.lastElementChild.innerText !==
      breadCrumList2.lastElementChild.inn
    ) {
      pollFor(".productGridItem", applyChanges);
    }

    function applyChanges() {
      let customGrid = document.querySelector(".ddl_productContainer");

      if (!customGrid) {
        removeAll();
        initChanges();
      }
    }
  });

  mainContentObserver.observe(body, config);
}

function removeAll() {
  let labels = document.querySelector(".applied-filters-labels-wrap");
  let header = document.querySelector(".headerWrap");
  let backBtn = document.querySelector(".goBackBtn");

  let filtersBlockArr = [labels, header, backBtn];

  filtersBlockArr.forEach((item) => {
    if (item) {
      item.parentElement.removeChild(item);
    }
  });
}
