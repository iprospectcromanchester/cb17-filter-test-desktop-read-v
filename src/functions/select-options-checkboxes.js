import { toArray } from "./helpers";

export default function addCustomCheckboxes() {
  let boxes = toArray(document.querySelectorAll("input.js-apply-filter"));

  boxes.forEach((box) => {
    if (box.parentElement.querySelector(".ddl-checkMark")) return;
    box.insertAdjacentHTML(
      "afterend",
      `<span class="ddl-checkMark" data-parent-filter="${box.dataset.parentFilter}" data-value="${box.value}" data-disabled="${box.disabled}"></span>`
    );
  });
}
