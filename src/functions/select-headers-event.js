import { toArray, createElement } from "./helpers";

export default function addEventToSelectHeaders() {
  let form = document.querySelector("form[name = refine]");
  let lists = toArray(form.querySelectorAll(".refineListItem:not(.links)"));

  addEventToFromChildrenHeaders(lists);
  addEventToFirstHeader();
  closeOnClickOut()
}
function addEventToFromChildrenHeaders(lists) {
  lists.forEach((list) => {
    let header = list.querySelector(".itemHeader.js-expand-refine");

    if (!hasEvents(header)) {
      header.addEventListener("click", () => {
        let previous = toArray(document.querySelectorAll(".ddl-last"));
        let options = list.querySelector("div.refineItemOptions");
        let optionsAndLinks = list.querySelector(".refineItemOptions");
        let arrow = header.querySelector(".ddl_arrow");

        if (previous.length > 0)
          previous.forEach((prev) => prev.classList.remove("ddl-last"));

        [arrow, optionsAndLinks].forEach((current) =>
          current.classList.add("ddl-last")
        );

        closeAll();
        toggleOptionsOpen(list, options, header);
      });
    }

    header.classList.add("with-events");
  });
}

function hasEvents(header) {
  return header.classList.contains("with-events");
}

function addEventToFirstHeader() {
  let headerParent = document.querySelector(".refineListItem.links");
  let header = headerParent.querySelector(".itemHeader");
  let options = document.querySelector("ul.refineItemOptions");

  if (!hasEvents(header)) {
    header.addEventListener("click", () => {
      let previous = toArray(document.querySelectorAll(".ddl-last"));
      if (previous.length > 0) {
        previous.forEach((prev) => prev.classList.remove("ddl-last"));
      }
      options.classList.add("ddl-last");
      closeAll();
      toggleOptionsOpen(headerParent, options, header);
    });
  }

  header.classList.add("with-events");
}

function toggleOptionsOpen(list, options, header) {
  let icon = list.querySelector(".ddl_arrow");

  if (!icon) {
    addIcon(header);
  }

  icon = list.querySelector(".ddl_arrow");

  options.classList.toggle("ddl-open");
  icon.classList.toggle("ddl-rotate");
  header.classList.toggle("ddl-filter-header-active");
}

function addIcon(header) {
  let icon = createElement("i", ["ddl_arrow", "ddl-rotate"]);
  icon.innerHTML = `<?xml version="1.0" encoding="UTF-8"?>
  <svg width="12px" height="20px" viewBox="0 0 12 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <!-- Generator: Sketch 55.2 (78181) - https://sketchapp.com -->
      <title>Path 2</title>
      <desc>Created with Sketch.</desc>
      <defs>
          <path d="M6,3.47637239 C6,3.47637239 6.51457807,2.98424826 7.54373421,2 L18,12 L7.54373421,22 L6,20.5236276 L14.9125316,12 C8.97084386,6.31758159 6,3.47637239 6,3.47637239 Z" id="path-1"></path>
      </defs>
      <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="💟-Icons/i-link-forward" transform="translate(-6.000000, -2.000000)">
              <mask id="mask-2" fill="white">
                  <use xlink:href="#path-1"></use>
              </mask>
              <use id="Path-2" fill="#373A3C" fill-rule="nonzero" xlink:href="#path-1"></use>
          </g>
      </g>
  </svg>`;
  header.appendChild(icon);
}

function closeOnClickOut(){

window.addEventListener("click", (e)=>{if(e.target.className.indexOf("temHeader") === -1 && e.target.dataset.parentFilter === undefined){
 
  let previous = toArray(document.querySelectorAll(".ddl-last"));
  if (previous.length > 0) {
    previous.forEach((prev) => prev.classList.remove("ddl-last"));
  }
  closeAll()
}})

}

function closeAll() {
  let opened = document.querySelectorAll(".ddl-open:not(.ddl-last)");
  let rotated = document.querySelectorAll(".ddl-rotate:not(.ddl-last)");


  if (opened.length > 0) {
    opened.forEach((list) => {
      list.classList.remove("ddl-open");
      list.previousElementSibling.classList.remove("ddl-filter-header-active");
    });
    rotated.forEach((arrow) => arrow.classList.remove("ddl-rotate"));
  }
}
