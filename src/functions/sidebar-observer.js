import { pollFor } from "icarus";
import { toArray, scrollToFilters } from "./helpers";
import { addEventToSelectOptions } from "./select-options-event";
import addEventToSelectHeaders from "./select-headers-event";
import addAppliedAmountToHeader from "./select-headers-numbers";
import addClearAllBtnToOptionLists from "./select-options-clear-all-btn";
import changeArrows from "./change-arrows";
import addCustomCheckboxes from "./select-options-checkboxes"

export default function startSidebarObserver() {
  let sidebar = document.querySelector(".sidebar");
  const config = { attributes: false, childList: true, subtree: true };

  const firstSelectObserver = new MutationObserver((value) => {
    value.forEach((record) => {
      let addedNodesClassNames = toArray(record.addedNodes).map((node) => {
        return node.className;
      });
      if (addedNodesClassNames.indexOf("refine js-panel") > -1) {
        pollFor("div.refineItemOptions", addClearAllBtnToOptionLists);
        pollFor(".icon-plus", changeArrows);
        addAppliedAmountToHeader();
        addEventToSelectHeaders();
        pollFor("input.js-apply-filter",addCustomCheckboxes)


        pollFor("input.js-apply-filter", () => {
          addEventToSelectOptions(document.querySelector(".col.mainContent"));
        });
        scrollToFilters()
      }
    });
  });

  firstSelectObserver.observe(sidebar, config);
}
