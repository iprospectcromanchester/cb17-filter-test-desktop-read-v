import createFiltersHeader from "./filters-header";
import { addToParent, createElement } from "./helpers";
import {
  createAppliedFiltersLabels,
  getAppliedFilters,
} from "./applied-filters-labels";
import createBackToPeviuosItemLink from "./back-to-prev-item-link";

export default function createFiltersBlock(mainContentWrap) {
  let mainContent = document.querySelector(".col.mainContent");
  let header = createFiltersHeader();
  let appliedFilterLabels = createAppliedFiltersLabels(getAppliedFilters());
  let backBtn = createBackToPeviuosItemLink();
  let shortIntro = document.querySelector(".js-category-intro-short");
  let filterBarPlaceHolder = createElement("div", "filter-bar-place-holder");

  addToParent([filterBarPlaceHolder, header, shortIntro], mainContentWrap);
  addToParent([appliedFilterLabels], mainContent);

  if (backBtn) {
    mainContentWrap.insertBefore(backBtn, mainContent);
  }
}
