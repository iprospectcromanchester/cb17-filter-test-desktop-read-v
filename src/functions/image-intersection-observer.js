//let IntersectionObserver = require("intersection-observer")



function createLazyImgObserver(lazyImages) {
  let config = {
    root: null,
    rootMargin: "300px",
    threshold: 0,
  };

  const lazyImgObserver = new IntersectionObserver(
    (entries, lazyImgObserver) => {
      entries.forEach((entry) => {
        if (!entry.isIntersecting) {
          return;
        } else {
          loadImage(entry.target);
          lazyImgObserver.unobserve(entry.target);
        }
      });
    },
    config
  );

  lazyImages.forEach((img) => {
    lazyImgObserver.observe(img);
  });
}

function loadImage(img) {
  let src = img.getAttribute("data-src");
  let srcRetina = img.getAttribute("data-src-retina");
  img.setAttribute("src", src);
  img.setAttribute("src-retina", srcRetina);
  img.classList.remove("lazyUnveil");
}

export { createLazyImgObserver, loadImage };
