import { toArray, createElement } from "./helpers";
import { loadImage } from "./image-intersection-observer";
import {
  hideLastRowIfIncomplete,
  updateLoadMoreItemsBtn,
} from "./product-grid-observer";
import { pollFor } from "icarus";

function addCustomProductGrid() {
  const grid = document.querySelector(".productGrid");

  const ddl_productContainer = createElement("div", "ddl_productContainer");
  ddl_productContainer.id = "grid-top"
  grid.insertAdjacentElement("beforebegin", ddl_productContainer);
  updateProductGrid(ddl_productContainer, grid);
}

function updateProductGrid(customGrid, OriginalGrid) {
  const products = toArray(OriginalGrid.querySelectorAll(".productGridItem"));

  products.forEach((prod, i) => {
    let image = prod.querySelector(".lazyUnveil");

    if (image && i < 30) {
      loadImage(image);
    }
    customGrid.appendChild(prod);
  });
  hideLastRowIfIncomplete(customGrid);
  pollFor(".btn.loadMoreProducts", updateLoadMoreItemsBtn);
}

export { addCustomProductGrid, updateProductGrid };
