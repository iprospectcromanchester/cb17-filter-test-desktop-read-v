function createElement(tag, cls = false) {
  let elem = document.createElement(tag);
  if (cls) {
    Array.isArray(cls)
      ? cls.forEach((cl) => {
          elem.classList.add(cl);
        })
      : elem.classList.add(cls);
  }
  return elem;
}

function toArray(nodeList) {
  return Array.prototype.slice.call(nodeList);
}

function addToParent(arr, parent) {
  arr.forEach((elem) => parent.insertBefore(elem, parent.firstChild));
}

function sort(parent) {
  let children = toArray(parent.children);
  let childrenNames = children
    .map((child) => {
      return child.innerText;
    })
    .sort();
  let orderedCHildren = [];
  childrenNames.forEach((name) => {
    orderedCHildren.push(
      children.filter((elem) => {
        return elem.innerText === name;
      })[0]
    );
  });
  orderedCHildren.forEach((child) => {
    parent.appendChild(child);
  });

  return parent;
}

function isIE() {
  let ua = navigator.userAgent;
  let is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

  return is_ie;
}

function appExists() {
  if (typeof app === "undefined") {
    return false;
  } else {
    return true;
  }
}

function scrollToFilters() {
  console.log(window);
  if (!window) return;
  const quickLinks = document.querySelector(".ddl_section-inner");

  if (quickLinks) {
    window.scrollTo(0, 660);
  } else {
    window.scrollTo(0, 460);
  }
}

function isSafari() {
  let ua = navigator.userAgent.toLowerCase();
  if (ua.indexOf("safari") != -1) {
    if (ua.indexOf("chrome") > -1) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
}

export {
  createElement,
  toArray,
  addToParent,
  sort,
  isIE,
  appExists,
  scrollToFilters,
  isSafari,
};
