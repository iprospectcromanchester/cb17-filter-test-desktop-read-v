import { createElement } from "./helpers";

export default function createFiltersHeader() {
  let headerWrap = createElement("div", "headerWrap");

  headerWrap.innerHTML = `<h3><span class="bigCurly">REFINE RESULTS</span></h3>`
    return headerWrap;
}
