import {
  getAppliedFilters,
  createAppliedFiltersLabels,
} from "./applied-filters-labels";
import { toArray } from "./helpers";
import { pollFor } from "icarus";

function addEventToSelectOptions(mainContent) {
  let form = document.querySelector("form[name=refine]");
  let optionsLabel = toArray(form.querySelectorAll("label"));
  let optionsInput = toArray(form.querySelectorAll("input[type=checkbox]"));
  let options = optionsLabel.concat(optionsInput);

  options.forEach((list) => {
    list.addEventListener("click", (e) => {
      if (e.target.classList.contains("inactive") || e.target.disabled) {
        return;
      }
      if (e.target.dataset.disabled === "true") {
        return;
      }
      if (e.clientX !== 0 && e.target) {
        let labelValue;

        if (e.target.dataset.value === undefined) {
          labelValue = e.target.value || e.target.firstElementChild.value;
        } else {
          labelValue = e.target.dataset.value;
        }

        let appliedFilters = updateAppliedFilters(labelValue);

        pollFor(
          "col.mainContent",
          appendLabelWrap(appliedFilters, mainContent)
        );
      }
    });
  });
}

function updateAppliedFilters(labelValue) {
  let appliedFilters = getAppliedFilters();

  appliedFilters.indexOf(labelValue) > -1
    ? (appliedFilters = appliedFilters.filter((val) => {
        return val !== labelValue;
      }))
    : appliedFilters.push(labelValue);
  

  return appliedFilters;
}

function appendLabelWrap(appliedFilters, mainContent) {
  let labelWrap = createAppliedFiltersLabels(appliedFilters);
  mainContent.replaceChild(labelWrap, mainContent.firstChild);
}

export { addEventToSelectOptions, appendLabelWrap };
