import { toArray, createElement } from "./helpers";

export default function changeArrows() {
  let arrows = toArray(document.querySelectorAll("i.icon-plus"));
  let firstHeader = document.querySelector(".itemBack");

  let arrowSvg = `<?xml version="1.0" encoding="UTF-8"?>
    <svg width="12px" height="20px" viewBox="0 0 12 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 55.2 (78181) - https://sketchapp.com -->
        <title>Path 2</title>
        <desc>Created with Sketch.</desc>
        <defs>
            <path d="M6,3.47637239 C6,3.47637239 6.51457807,2.98424826 7.54373421,2 L18,12 L7.54373421,22 L6,20.5236276 L14.9125316,12 C8.97084386,6.31758159 6,3.47637239 6,3.47637239 Z" id="path-1"></path>
        </defs>
        <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="💟-Icons/i-link-forward" transform="translate(-6.000000, -2.000000)">
                <mask id="mask-2" fill="white">
                    <use xlink:href="#path-1"></use>
                </mask>
                <use id="Path-2" fill="#373A3C" fill-rule="nonzero" xlink:href="#path-1"></use>
            </g>
        </g>
    </svg>`;

  if (firstHeader) {
    let span = firstHeader.querySelector("span.refineListItemHeader");
    let i = createElement("i", "ddl_arrow");
    i.innerHTML = arrowSvg;
    span.insertAdjacentElement("afterend", i);
  }

  arrows.forEach((arrow) => {
    arrow.classList.remove("icon-plus");
    arrow.classList.add("ddl_arrow");

    arrow.innerHTML = arrowSvg;
  });
}
