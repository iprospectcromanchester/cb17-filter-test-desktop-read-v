import { createElement, toArray, sort, appExists } from "./helpers";
import DDLtrackCampaign from "../common";
import { pollFor } from "icarus";

let labelsWrap;

function createAppliedFiltersLabels(appliedFilters) {
  labelsWrap = createElement("div", "applied-filters-labels-wrap");

  if (appliedFilters.length > 0) {
    appliedFilters.forEach((filterValue) => {
      addNewLabel(filterValue);
    });

    //Uncomment for alphabetically ordered labels

    //let wrapSorted = sort(labelsWrap);
    //wrapSorted.appendChild(createClearFiltersBtn());
    //return wrapSorted;

    labelsWrap.appendChild(createClearFiltersBtn());

    return labelsWrap;
  }

  return labelsWrap;
}

function addNewLabel(filterValue) {
  labelsWrap.appendChild(createLabel(filterValue));
}
function createClearFiltersBtn() {
  let btn = createElement("div", "clearFiltersBtn");

  btn.innerText = "Clear all ";

  btn.addEventListener("click", (e) => {
    DDLtrackCampaign("Variation 1", "Clear all Clicked", "CB17 - V1");
    getAppliedFilters().forEach((filterValue) => {
      clearFilter(filterValue);
    });

    if (getAppliedFilters().length > 0) {
      pollFor(appExists, app.clearFilters);
    }
  });
  return btn;
}

function createLabel(filterValue) {
  let text = getLabelText(filterValue); 

  let dataParent = getDataParent(filterValue);
  let filterLabel = createElement("div", "filterLabel");

  filterLabel.innerHTML = text;
  filterLabel.dataset.value = filterValue;
  filterLabel.dataset.parent = dataParent;

  filterLabel.addEventListener("click", (e) => {
    let checkBoxValue = e.target.dataset.value;
    clearFilter(checkBoxValue, labelsWrap);
  });
  return filterLabel;
}

function clearFilter(filterValue) {
  let parallelCheckBox = getParallelCheckBox(filterValue);
  if (!parallelCheckBox.disabled) {
    parallelCheckBox.click();
    removeLabel(filterValue, labelsWrap);
  }
}

function removeLabel(filterValue) {
  let label = toArray(labelsWrap.children).filter((label) => {
    return label.dataset.value === filterValue;
  })[0];

  labelsWrap.removeChild(label);

  if (getAppliedFilters().length < 1) {
    let clearAll = document.querySelector(".clearFiltersBtn");
    clearAll.parentElement.removeChild(clearAll);
  }
}

function getLabelText(filterValue) {
  let label = getParallelCheckBox(filterValue).parentElement;
  let text = label.childNodes[3].textContent;


  console.log(getParallelCheckBox(filterValue))


  return text.replace(/\(/g, "    (");
}

function getDataParent(filterValue) {
  let dataParent = getParallelCheckBox(filterValue)
    .dataset.parentFilter.toLowerCase()
    .replace(/\s/g, "_");

  return dataParent;
}

function getParallelCheckBox(filterValue) {
  let checkedBoxes = toArray(document.querySelectorAll(".js-apply-filter"));

  return checkedBoxes.filter((box) => {
    return box.value === filterValue;
  })[0];
}

function getAppliedFilters() {
  let appliedObj = app.state.appliedFilters;
  let appliedArr = [];
  for (const filterName in appliedObj) {
    let value = appliedObj[filterName];

    if (/^(?:\d+,)*\d+$/.test(value)) {
      if (value.indexOf(",") > -1) {
        let subArr = value.split(",");
        subArr.forEach((name) => {
          appliedArr.push(name);
        });
      } else {
        appliedArr.push(value);
      }
    }
  }

  return appliedArr;
}

export { createAppliedFiltersLabels, getAppliedFilters, getParallelCheckBox };
