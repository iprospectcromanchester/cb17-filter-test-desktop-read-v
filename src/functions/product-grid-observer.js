import { toArray } from "./helpers";
import { loadImage } from "./image-intersection-observer";
import { pollFor } from "icarus";

function startGridObserver() {
  const grid = document.querySelector(".productGrid");
  let customGrid = document.querySelector(".ddl_productContainer");
  const config = { attributes: false, childList: true, subtree: false };

  const productGridObserver = new MutationObserver((value) => {
    updateGrid(value[0], grid, customGrid);
    hideLastRowIfIncomplete(customGrid);
  });
  startLoadMoreButtonObserver();
  productGridObserver.observe(grid, config);
}

function startLoadMoreButtonObserver() {
  if (
    !document.querySelector(
      ".loadMoreProductsContainer" 
    ) || !document.querySelector(
      ".loadMoreProductsContainer" 
    ).firstElementChild
  )
    return;

  const button = document.querySelector(".loadMoreProductsContainer");
  const config = { attributes: false, childList: true, subtree: true };
  let itemsCountText = button.firstElementChild.innerText;

  const loadMoreBtnObserver = new MutationObserver((value) => {
    let addedNode = value.filter((record) => {
      return record.addedNodes.length > 0;
    })[0];
    let textAdded = addedNode.addedNodes[0].data;
    let countBeforeChange = itemsCountText.match(/\d+/)[0];
    let countAfterChange = textAdded.match(/\d+/)[0];

    if (countBeforeChange !== countAfterChange) {
      pollFor(".btn.loadMoreProducts", updateLoadMoreItemsBtn);
      loadMoreBtnObserver.disconnect();
    } else {
      itemsCountText = textAdded;
    }
  });

  loadMoreBtnObserver.observe(button, config);
}

function updateGrid(value, grid, customGrid) {
  let products = toArray(grid.querySelectorAll(".productGridItem"));

  if (value.removedNodes.length > 0) {
    clearGrid(customGrid);
  }

  appendProducts(products, customGrid);
}

function clearGrid(customGrid) {
  customGrid.innerHTML = "";
}

function appendProducts(products, customGrid) {
  products.forEach((prod, i) => {
    let image = prod.querySelector(".lazyUnveil");
    if (i < 16) {
      loadImage(image);
    }
    hideLastRowIfIncomplete(customGrid);
    customGrid.appendChild(prod);
  });
}

function hideLastRowIfIncomplete(customGrid) {
  let currentProdList = toArray(
    customGrid.querySelectorAll(".productGridItem")
  );
  let allItemsAreAppended = currentProdList.length % 30 === 0;
  if (allItemsAreAppended) {
    let lastRowItemsCountText = currentProdList.length % 4;
    let lastRowItems = currentProdList.splice(-lastRowItemsCountText);

    if (lastRowItems.length > 0 && lastRowItems.length <= 3) {
      lastRowItems.forEach((item) => item.classList.toggle("ddl_hide_product"));
    }
  }
}

function updateLoadMoreItemsBtn() {
  let button = document.querySelector(".btn.loadMoreProducts");
  let loadMoreCountNum = parseInt(button.innerText.match(/\d+/));

  let hiddenItemsCountText =
    document.querySelectorAll(".ddl_hide_product").length || 0;
  let total = loadMoreCountNum + hiddenItemsCountText;

  if (total !== NaN) {
    document.querySelector(
      ".btn.loadMoreProducts"
    ).innerText = button.innerText.replace(/\d+/, `${total}`);
    button.classList.add(".updatedCount");
  }
  return total;
}

export { startGridObserver, hideLastRowIfIncomplete, updateLoadMoreItemsBtn };
