import {isSafari} from "./helpers"

export default function hideLabelsInSafariHashBack(){
    let noCheckedBoxes = document.querySelectorAll("input.js-apply-filter[checked]").length === 0;
    let labelsWrap = document.querySelector(".applied-filters-labels-wrap")
    if(isSafari && noCheckedBoxes){
       labelsWrap.innerHTML = ""
    }

}