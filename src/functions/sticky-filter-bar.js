export default function stickyScrollObserver() {
  let filterBar = document.querySelector(".sidebar");
  document.addEventListener("scroll", () => {
    filterBarIsFixed()
      ? toggleWideClass(filterBar, true)
      : toggleWideClass(filterBar, false);
  });
}

function toggleWideClass(filterBar, add) {
  add
    ? checkWidthNeeded(filterBar)
    : filterBar.classList.remove("wide-filter-bar");
}

function checkWidthNeeded(filterBar) {
  let barLength = filterBar.querySelector("form").children.length;

  barLength > 3
    ? filterBar.classList.add("wide-filter-bar")
    : ["wide-filter-bar", "short-bar"].forEach((cls) =>
        filterBar.classList.add(cls)
      );
}

function filterBarIsFixed() {
  let filterBarPLaceHolder = document.querySelector(".filter-bar-place-holder");
  return filterBarPLaceHolder.getBoundingClientRect().top < 0;
}
