import DDLtrackCampaign from "./common.js";
import { pollFor } from "icarus";
import initCommonTracking from "./tracking/common_tracking";

const testVar = "Control";
const label = "CB17 - Control";

pollFor("body", initCB17);

function initCB17() {
  if (document.body.className.indexOf("CB17_loaded") === -1) {
    DDLtrackCampaign(testVar); // general campaign tracking
    CB17Changes();
    initCommonTracking(testVar, label);
  } else {
    console.warn("Experiment not loaded");
  }
}

function CB17Changes() {
  document.body.classList.add("CB17_loaded");
}
